//
//  VariantChoice.h
//  Variant
//
//  Created by Andrey Mishanin on 13/05/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#ifndef VariantChoice_h
#define VariantChoice_h

#include "Typelist.h"

template <typename... Types>
class Variant;

template <typename T, typename... Types>
class VariantChoice {
  using Derived = Variant<Types...>;
  Derived& getDerived() { return *static_cast<Derived*>(this); }
  Derived const& getDerived() const { return *static_cast<Derived const*>(this); }

protected:
  static constexpr unsigned Discriminator = FindIndexOfT<Typelist<Types...>, T>::value + 1;

public:
  VariantChoice() { }
  VariantChoice(T const& value);
  VariantChoice(T&& value);
  bool destroy();
  Derived& operator= (T const& value);
  Derived& operator= (T&& value);
};

#endif /* VariantChoice_h */
