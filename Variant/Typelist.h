//
//  Typelist.h
//  Variant
//
//  Created by Andrey Mishanin on 07/05/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#ifndef Typelist_h
#define Typelist_h

#include "IfThenElse.h"

#include <type_traits>

template <typename... Elements>
class Typelist {};

#pragma mark - Front

template <typename List>
class FrontT;

template <typename Head, typename... Tail>
class FrontT<Typelist<Head, Tail...>> {
public:
  using Type = Head;
};

template <typename List>
using Front = typename FrontT<List>::Type;

#pragma mark - PopFront

template <typename List>
class PopFrontT;

template <typename Head, typename... Tail>
class PopFrontT<Typelist<Head, Tail...>> {
public:
  using Type = Typelist<Tail...>;
};

template <typename List>
using PopFront = typename PopFrontT<List>::Type;

#pragma mark - PushFront

template <typename List, typename NewElement>
class PushFrontT;

template <typename... Elements, typename NewElement>
class PushFrontT<Typelist<Elements...>, NewElement> {
public:
  using Type = Typelist<NewElement, Elements...>;
};

template <typename List, typename NewElement>
using PushFront = typename PushFrontT<List, NewElement>::Type;

#pragma mark - NthElement

template <typename List, int N>
class NthElementT: public NthElementT<PopFront<List>, N - 1> {
};

template <typename List>
class NthElementT<List, 0>: public FrontT<List> {
};

template <typename List, int N>
using NthElement = typename NthElementT<List, N>::Type;

#pragma mark - IsEmpty

template <typename List>
class IsEmpty {
public:
  static constexpr bool value = false;
};

template <>
class IsEmpty<Typelist<>> {
public:
  static constexpr bool value = true;
};

#pragma mark - FindIndexOf

template <typename List, typename T, unsigned N = 0, bool Empty = IsEmpty<List>::value>
struct FindIndexOfT;

// Recursive case
template <typename List, typename T, unsigned N>
struct FindIndexOfT<List, T, N, false>: public IfThenElse<std::is_same<Front<List>, T>::value, std::integral_constant<unsigned, N>, FindIndexOfT<PopFront<List>, T, N + 1>> { };

// Basis case
template <typename List, typename T, unsigned N>
struct FindIndexOfT<List, T, N, true> {};

#endif /* Typelist_h */
