//
//  main.cpp
//  Variant
//
//  Created by Andrey Mishanin on 07/05/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#include <iostream>
#include <new>

#include "LargestType.h"
#include "Typelist.h"
#include "Variant.h"

template <typename T>
class TypeDebugger;

#define RevealType(T) auto __td = TypeDebugger<T>()

int main(int argc, const char * argv[]) {
  using SignedIntegralTypes = Typelist<signed char, short, int, long, long long>;
  using First = Front<SignedIntegralTypes>;
  using Rest = PopFront<SignedIntegralTypes>;
  using Append = PushFront<SignedIntegralTypes, bool>;
  using Replace = PushFront<PopFront<SignedIntegralTypes>, bool>;
  using Third = NthElement<SignedIntegralTypes, 2>;
  using Largest = LargestType<SignedIntegralTypes>;
//  RevealType(Largest);
}
