//
//  VariantGet.cpp
//  Variant
//
//  Created by Andrey Mishanin on 19/05/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#include "Variant.h"

#include <exception>

class EmptyVariant: public std::exception {};

template <typename... Types>
template <typename T>
T& Variant<Types...>::get() & {
  if (isEmpty()) {
    throw EmptyVariant {};
  }

  assert(is<T>());
  return *this->template getBufferAs<T>();
}
