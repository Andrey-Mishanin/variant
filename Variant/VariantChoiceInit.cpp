//
//  VariantChoiceInit.cpp
//  Variant
//
//  Created by Andrey Mishanin on 19/05/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#include "VariantChoice.h"

#include <utility>

template <typename T, typename... Types>
VariantChoice<T, Types...>::VariantChoice(T const& value) {
  new(getDerived().getRawBuffer()) T(value);
  getDerived().setDiscriminator(Discriminator);
}

template <typename T, typename... Types>
VariantChoice<T, Types...>::VariantChoice(T&& value) {
  new(getDerived().getRawBuffer()) T(std::move(value));
  getDerived().setDiscriminator(Discriminator);
}
