//
//  VariantDestroy.cpp
//  Variant
//
//  Created by Andrey Mishanin on 19/05/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#include "Variant.h"

template <typename... Types>
void Variant<Types...>::destroy() {
  const bool results[] = {
    VariantChoice<Types, Types...>::destroy()...
  };
  this->setDiscriminator(0);
}
