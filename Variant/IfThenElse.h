//
//  IfThenElse.h
//  Variant
//
//  Created by Andrey Mishanin on 13/05/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#ifndef IfThenElse_h
#define IfThenElse_h

template <bool Condition, typename TrueType, typename FalseType>
struct IfThenElseT {
  using Type = TrueType;
};

template <typename TrueType, typename FalseType>
struct IfThenElseT<false, TrueType, FalseType> {
  using Type = FalseType;
};

template <bool Condition, typename TrueType, typename FalseType>
using IfThenElse = typename IfThenElseT<Condition, TrueType, FalseType>::Type;

#endif /* IfThenElse_h */
