//
//  VariantIs.cpp
//  Variant
//
//  Created by Andrey Mishanin on 19/05/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#include "Variant.h"

template <typename... Types>
template <typename T>
bool Variant<Types...>::is() const {
  return this->getDiscriminator() == VariantChoice<T, Types...>::Discriminator;
}
