//
//  VariantChoiceDestroy.cpp
//  Variant
//
//  Created by Andrey Mishanin on 19/05/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#include "Variant.h"

template <typename T, typename... Types>
bool VariantChoice<T, Types...>::destroy() {
  if (getDerived().getDiscriminator() == Discriminator) {
    getDerived().template getBufferAs<T>()->~T();
    return true;
  }
  return false;
}
