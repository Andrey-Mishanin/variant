//
//  Variant.h
//  Variant
//
//  Created by Andrey Mishanin on 19/05/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#ifndef Variant_h
#define Variant_h

#include "VariantStorage.h"
#include "VariantChoice.h"

template <typename... Types>
class Variant: private VariantStorage<Types...>, private VariantChoice<Types, Types...>... {
  template <typename T, typename... OtherTypes>
  friend class VariantChoice;

public:
  template <typename T>
  bool is() const;

  bool isEmpty() const;

  // C++17
  using VariantChoice<Types, Types...>::VariantChoice...;

  Variant();
  Variant(Variant const& source);
  Variant(Variant&& source);

  ~Variant() { destroy(); }
  void destroy();

private:
  template <typename T>
  T& get() &;
  template <typename T>
  T const& get() const&;
  template <typename T>
  T&& get() &&;

};

#endif /* Variant_h */
