//
//  VariantStorage.h
//  Variant
//
//  Created by Andrey Mishanin on 13/05/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#ifndef VariantStorage_h
#define VariantStorage_h

#include "LargestType.h"

using Byte = unsigned char;

template <typename... Types>
class VariantStorage {
  using LargestT = LargestType<Typelist<Types...>>;
  alignas(Types...) Byte buffer[sizeof(LargestT)];
  Byte discriminator = 0;

public:
  Byte getDiscriminator() const { return discriminator; }
  void setDiscriminator(Byte d) { discriminator = d; }
  void* getRawBuffer() { return buffer; }
  const void* getRawBuffer() const { return buffer; }

  template <typename T>
  T* getBufferAs() { return reinterpret_cast<T*>(buffer); }

  template <typename T>
  T const* getBufferAs() const { return reinterpret_cast<T const*>(buffer); }
};

#endif /* VariantStorage_h */
