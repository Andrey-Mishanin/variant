//
//  LargestType.h
//  Variant
//
//  Created by Andrey Mishanin on 13/05/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#ifndef LargestType_h
#define LargestType_h

#include "Typelist.h"

template <typename List, bool Empty = IsEmpty<List>::value>
class LargestTypeT;

// Recursive case
template <typename List>
class LargestTypeT<List, false> {
private:
  using Contender = Front<List>;
  using Best = typename LargestTypeT<PopFront<List>>::Type;

public:
  using Type = IfThenElse<(sizeof(Contender) >= sizeof(Best)), Contender, Best>;
};

// Basis case
template <typename List>
class LargestTypeT<List, true> {
public:
  using Type = char;
};

template <typename List>
using LargestType = typename LargestTypeT<List>::Type;

#endif /* LargestType_h */
